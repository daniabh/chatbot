import numpy as np
import nltk

from nltk.stem.porter import PorterStemmer
stemmer = PorterStemmer()


def tokenize(sentence):
    return nltk.word_tokenize(sentence)

def stem(word):
    return stemmer.stem(word.lower())

def bag_of_words(tokenized_sentence,words):
    # Stem each word
    sentence_words = [stem(word) for word in tokenized_sentence]
    # Initialize the bag with 0 for each word
    bag = np.zeros(len(words), dtype=np.float32)
    for index, word in enumerate(words):
        if word in sentence_words:
            bag[index] = 1

    return bag


words = ["organize","organizes","organizing"]

stemmed_words = [stem(w) for w in words]
print(stemmed_words)

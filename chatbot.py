from flask import Flask, render_template, request, redirect, url_for, flash, session
from werkzeug.security import generate_password_hash, check_password_hash
import os
import sqlite3
import random
import json
import torch
import webbrowser
import requests
import time
from datetime import datetime
from pygame import mixer
from billboard import ChartData
from nltk_utils import bag_of_words, tokenize

from cleaner import clean_corpus
from db import create_tables, drop_tables
from model import NeuralNet
import execjs

if __import__("sys").platform == "emscripten":
    from platform import window

CORPUS_FILE = "chat.txt"

# Initialize Flask application
app = Flask(__name__)
app.secret_key = 'your_secret_key_here'

drop_tables()
# Create tables in the database
create_tables()

# Initialize the new chatbot
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

with open('intents.json', 'r') as json_data:
    intents = json.load(json_data)

FILE = "data.pth"
data = torch.load(FILE)

input_size = data["input_size"]
hidden_size = data["hidden_size"]
output_size = data["output_size"]
all_words = data['all_words']
tags = data['tags']
model_state = data["model_state"]

model = NeuralNet(input_size, hidden_size, output_size).to(device)
model.load_state_dict(model_state)
model.eval()



# Refactored get_bot_response method
def get_bot_response(user_text):
    # Tokenize and convert to bag of words
    sentence = tokenize(user_text)
    X = bag_of_words(sentence, all_words)
    X = X.reshape(1, X.shape[0])
    X = torch.from_numpy(X).to(device)

    # Get model prediction
    output = model(X)
    _, predicted = torch.max(output, dim=1)
    tag = tags[predicted.item()]

    # Check if tag is in the specified categories, otherwise use default response
    if tag in ['google', 'weather', 'news', 'song']:
        bot_response = handle_special_responses(tag, user_text)
    else:
        bot_response = handle_default_response(tag)


    # Get current time in ISO format
    currentTime = datetime.now().isoformat()


     # Save message to database
    save_message_to_database(session['user_id'],'user', user_text)
    save_message_to_database(session['user_id'],'bot', bot_response)

    return bot_response


# Helper method to handle specific types of responses
def handle_special_responses(tag, user_text):
    if tag == 'google':
        query = user_text  # Use user's query directly
        webbrowser.open(f"https://google.com/search?q={query}")
        return "Opening Google search for you..."
    elif tag == 'weather':
        api_key = '987f44e8c16780be8c85e25a409ed07b'
        base_url = "http://api.openweathermap.org/data/2.5/weather?"
        city_name = user_text  # Use user's input directly as city name
        complete_url = f"{base_url}appid={api_key}&q={city_name}"
        response = requests.get(complete_url)
        x = response.json()
        temperature = round(x['main']['temp'] - 273, 2)
        feels_like = round(x['main']['feels_like'] - 273, 2)
        weather_main = x['weather'][0]['main']
        return f"Current temperature: {temperature}°C, Feels like: {feels_like}°C. Weather: {weather_main}."
    elif tag == 'news':
        main_url = "http://newsapi.org/v2/top-headlines?country=us&apiKey=bc88c2e1ddd440d1be2cb0788d027ae2"
        open_news_page = requests.get(main_url).json()
        articles = open_news_page["articles"]
        results = []
        for article in articles[:5]:  # Show top 5 articles
            results.append(f"{article['title']} - {article['url']}")
        return "\n\n".join(results)
    elif tag == 'song':
        chart = ChartData('hot-100')
        songs = [f"{song.title} - {song.artist}" for song in chart[:10]]
        return "\n\n".join(songs)
    else:
        return "Sorry, I can't understand that right now."


# Helper method to handle default response
def handle_default_response(tag):
    for intent in intents['intents']:
        if tag == intent["tag"]:
            return random.choice(intent['responses'])
    return "I'm not sure I understand. Could you ask me something else?"



# Helper method to save message to database
def save_message_to_database(user_id, sender, message):
    conn = sqlite3.connect('chatbot.db')
    cursor = conn.cursor()
    cursor.execute('''
        INSERT INTO messages (user_id, sender, message, timestamp)
        VALUES (?, ?, ?, ?)
    ''', (user_id, sender, message, datetime.now().isoformat()))
    conn.commit()
    conn.close()




# Helper method to retrieve messages for the current user from database
def get_user_messages_from_db(user_id):
    conn = sqlite3.connect('chatbot.db')
    cursor = conn.cursor()
    cursor.execute('''
        SELECT * FROM messages
        WHERE user_id = ?
        ORDER BY timestamp
    ''', (user_id,))
    messages = cursor.fetchall()
    conn.close()
    return messages



# Helper method to retrieve all messages from database
def get_all_messages_from_db():
    conn = sqlite3.connect('chatbot.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM messages ORDER BY timestamp')
    messages = cursor.fetchall()
    conn.close()
    return messages

# Home route - Renders index.html page if user is logged in
@app.route("/", methods=['GET', 'POST'])
def home():
    if 'user_id' in session:
        user = get_user_by_id(session['user_id'])
        chatmessages =  get_user_messages_from_db(session['user_id'])
        print(chatmessages)

         # Convert tuples to dictionaries and format timestamps
        formatted_messages = []
        for message in chatmessages:
            message_dict = {
                'id': message[0],
                'user_id': message[1],
                'sender': message[2],
                'message': message[3],
                'timestamp': message[4],
            }

             # Convert ISO format to datetime object
            dt_object = datetime.fromisoformat(message_dict['timestamp'])

            # Format datetime object to desired format (e.g., 10:30AM)
            formatted_time = dt_object.strftime("%I:%M%p")

            message_dict['timestamp'] = formatted_time

            formatted_messages.append(message_dict)

        return render_template("index.html", user=user, chatmessages=formatted_messages)
    else:
        return redirect(url_for('login'))



@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        # Check if user exists in the database
        user = get_user_by_email(email)

        if user and check_password_hash(user[2], password):  # Assuming password hash is at index 2
            session['user_id'] = user[0]  # Assuming user_id is at index 0
            session['user_name'] = user[3]
            session['profile_picture'] = user[4]

            # Initialize session['messages'] if not already initialized
            if 'messages' not in session:
                session['messages'] = []

            flash('Login successful!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Invalid email or password', 'error')

    return render_template('login.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        name = request.form['name']

        # Check if email already exists
        if get_user_by_email(email):
            print("user exists")
            flash('Email address already exists. Please use a different email.', 'error')
        else:

            # Hash the password (replace with actual hashing method)
            hashed_password = generate_password_hash(password)

            # Create new user
            user_id = create_user(email, hashed_password, name, profile_picture_filename)

            flash('Registration successful! Please log in.', 'success')
            return redirect(url_for('login'))

    return render_template('register.html')




# Chat route - Dummy route for chatbot responses
@app.route("/get", methods=['GET'])
def handle_get_request():
    user_text = request.args.get('msg')
    bot_response = get_bot_response(user_text)
    return bot_response

# Logout route
@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for('login'))  # Redirect to login page

def get_user_by_id(user_id):
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users WHERE id = ?', (user_id,))
    user = cursor.fetchone()
    conn.close()
    return user

def get_user_by_email(email):
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users WHERE email = ?', (email,))
    user = cursor.fetchone()
    conn.close()
    return user

def create_user(email, password, name, profile_picture):
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    cursor.execute('''
        INSERT INTO users (email, password, name)
        VALUES (?, ?, ?, ?)
    ''', (email, password, name, profile_picture))
    user_id = cursor.lastrowid
    conn.commit()
    conn.close()
    return user_id

if __name__ == "__main__":
    app.run(debug=True)



window.localStorage.setItem("test", "test" )

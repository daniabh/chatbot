document.addEventListener('DOMContentLoaded', function() {
    const userInput = document.getElementById('chat-input');
    const sendButton = document.getElementById('send-button');
    const chatMessages = document.getElementById('chat-messages');

    sendButton.addEventListener('click', function() {
        const userMessage = userInput.value.trim();

        if (userMessage !== '') {
            // Display user message
            displayMessage(userMessage, 'sent');

            // Send user message to server
            fetch(`/get?msg=${encodeURIComponent(userMessage)}`)
                .then(response => response.text())
                .then(botMessage => {
                    // Display bot response
                    displayMessage(botMessage, 'received');
                })
                .catch(error => console.error('Error sending message to server:', error));

            // Clear input after sending
            userInput.value = '';
        }
    });

    function displayMessage(message, sender) {
        const messageElement = document.createElement('div');
        messageElement.classList.add('message', sender);

        const messageContent = `
            <div class="message-content">
                <p>${message}</p>
                <span class="message-time">${formatMessageTime(new Date())}</span>
            </div>
        `;
        messageElement.innerHTML = messageContent;

        chatMessages.appendChild(messageElement);

        // Automatically scroll to the bottom of the chat messages
        chatMessages.scrollTop = chatMessages.scrollHeight;
    }

    function formatMessageTime(date) {
        let hours = date.getHours();
        const minutes = date.getMinutes().toString().padStart(2, '0');
        const period = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12 || 12; // Convert hours to 12-hour format
        return `${hours}:${minutes} ${period}`;
    }
});

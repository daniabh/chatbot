import sqlite3


# Function to drop tables
def drop_tables():
    try:
        # Connect to SQLite database
        conn = sqlite3.connect('chatbot.db')
        cursor = conn.cursor()

        # Drop users table
        cursor.execute('DROP TABLE IF EXISTS users;')
        print("Dropped users table.")

        # Drop messages table
        cursor.execute('DROP TABLE IF EXISTS messages;')
        print("Dropped messages table.")

        # Commit changes and close connection
        conn.commit()
        conn.close()
        print("Tables dropped successfully.")

    except sqlite3.Error as e:
        print(f"Error dropping tables: {e}")


def create_tables():
    conn = sqlite3.connect('chatbot.db')
    cursor = conn.cursor()

    # Create users table
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            email TEXT NOT NULL UNIQUE,
            password TEXT NOT NULL,
            name TEXT NOT NULL
        )
    ''')

    # Create messages table with user_id and sender fields
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS messages (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_id INTEGER NOT NULL,
            sender TEXT NOT NULL,
            message TEXT NOT NULL,
            timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY (user_id) REFERENCES users (id)
        )
    ''')

    conn.commit()
    conn.close()

if __name__ == "__main__":
    drop_tables()
    create_tables()
